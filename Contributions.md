# Contributions

Ibad and Nabeel Researched. Then along with Majid, we finalised the topic by choosing Off-by-one.

**Slides**: Ibad created the slides and added readme files in the repository.

**Program**: Nabeel created the program and the makefile.

**Recording**: Majid and Nabeel recorded the presentation. Majid also compiled the video with keeping the 30m restriction in mind.

All of this was done collectively with continuous discussions in the team through out the duration of the assignment.
