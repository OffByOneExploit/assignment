#include <stdio.h>
#include <string.h>

void cpy(char *x)
{
	char buff[1024];
	printf("%p\n", buff);
	strcpy(buff, x);
	printf("%s\r\n", buff);
}

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("Usage: ./offby <text string>\n");
		return 1;
	}

	if (strlen(argv[1]) > 1024)
	{
		printf("Buffer Overflow Attempt!!!\r\n");
		return 1;
	}
	cpy(argv[1]);
}
