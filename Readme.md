# Off-By-One Vulnerability

Off-by-one is a type of buffer overflow. Overflowing the buffer by one byte can lead to corrupting and modifying the address to point it to a malicious code. The malicious code will be executed and the exploit will give root access to the attacker.

## Setting the Environment

1.  Download the repository on your local machine.

2.  Install docker using the following command: sudo apt-get install docker

3. Open the terminal and run the Makefile inside the assignment folder.

4.  You should now get a shell with root@\<ID\>. Keep a copy of the ID.  

5.  Open another terminal and go to the assignment folder in the repository and run the following command: sudo docker cp src :/tmp/src

6.  Go back to the docker terminal and run the command cd /tmp

7.  In this folder you should see the src folder. Go inside it and you will find all the necessary files to run the exploit.

8.  Perform the chown command on offby file to make the owner and group root. Make the offby file executable by owner and also set the suid bit on.
    

## Executing the Program

- The unpatched program is vulnerable to off-by-one vulnerability. It takes 2 command line argument along with the program executable. Anything more or less than 2 will prompt the user to follow the format (2 arguments).

- Upon getting 2 arguments, the program will copy the argument and print it. The program has preventive measures for vanilla buffer overflow. Anything above 1024 bytes is handled through a "buffer overflow" output display. Anything below 1024 is copied into the array and printed. However, the unpatched program will run into segmentation fault if the command line argument is 1024 bytes long.  

- This segmentation fault will allow us to exploit the system and gain root access (assuming it is a root file and is run as root).


## Detection

This vulnerability can be detected by either static analysis or run time analysis.  

1. **Static Analysis:** This type of analysis can be done via code review and checking for extra loop iterations or checking for incorrect copy of string in the buffer, typically copying a the string that is one byte too long than the buffer.

2. **Run-time Analysis:** Perform tests (unit tests) on the program each time the code is pushed into the main repository. This will prevent push requests to the main repository upon failure of a test or tests.


## Exploitation

1. As mentioned above, if we enter 1024 bytes long argument then the program will show segmentation fault. This is because the program will add 1024 bytes and one null character (\x00). So the actual size being copied will be 1025 bytes whereas the buffer size is only 1024.

2. We are using a machine which uses little-endian. So, the least significant bit of the ebp will be changed to the null character value (00). This will make the ebp point down (lower address). So if the 1024 A's were the used as the input then the ebp will be pointing at a 0x41414141 address, and since this address can't be accessed, we get a segmentation fault.

3. Now we can take this to our advantage, by adding the *return address at the end of the buffer of the start of the buffer* and add the shellcode in the middle of the buffer instead of A's.

4. We'll do some calculations first, the size of the buffer is 1024 bytes. The shellcode that we have decided to use is 21 bytes long, so we are left with 1003 bytes to fill. The return address is 4 bytes long and to increase the chances of the ebp pointing at the address, we'll add return address 100 times, so 4*100 will equate to 400 bytes. Now we still have 603 bytes. We'll add 603 NOP. NOP means nothing and it continues the operation to the next instruction. So after hitting the NOP, it will hit and execute our shellcode which will get us a new shell with root access. 

Shellcode: http://shell-storm.org/shellcode/files/shellcode-841.php

Example of what the command might look like in the end: ./offby $(python -c "print('\x90'*603 + '\x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80' + '\x6c\xec\xff\xbf'*100)")
    
**NOTE**: You will likely get a segmentation fault. gdb will allow us to find the start of buffer but for simplicity, check the address the program outputs after running and replace the last address in the above command with it (after reversing the address). E.g. if the program outputs 0xffffd0ec, you should reverse the address in the argument: $(python -c "print('\x90'*603 + '\x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80' + '\xec\xd0\xff\xff'*100)")


## Patch

1. During the checking of the size of the buffer, 1024 + null character was being skipped so to fix that, we have added an equal sign which will prevent the user from entering an argument of size 1024 or more. 

2. Replaced stcpy() with strncpy(). strncpy() requires an additional parameter, the size of the buffer, therefore preventing any extra number of bytes being copied into the buffer.
